#!/usr/bin/env bash

OUTPUT_FILENAME=beamer_theme_innovenso
OUTPUT_VERSION=0.0.1
ROOT_DIR=`pwd`
BUILD_DIR=${ROOT_DIR}/build
OUTPUT_DIR=${ROOT_DIR}/output
PACKAGE_DIR=${BUILD_DIR}/package
SRC_DIR=${ROOT_DIR}/src
INPUT_FILE=main

function clean {
	rm -rf ${BUILD_DIR}
	rm -rf ${OUTPUT_DIR}
}

function build {
	determineVersion
	rm -rf ${BUILD_DIR}
	mkdir -p ${BUILD_DIR}
	cp -R ${SRC_DIR}/* ${BUILD_DIR}
	cd ${BUILD_DIR}
	latexmk -pdf ${INPUT_FILE}.tex
	cd ${ROOT_DIR}
	mkdir ${OUTPUT_DIR}
	cp $BUILD_DIR/${INPUT_FILE}.pdf ${OUTPUT_DIR}/${OUTPUT_FILENAME}-${OUTPUT_VERSION}.pdf
	rm -rf ${BUILD_DIR}
}

function determineVersion {
	LATEST_REVISION=`git rev-list --tags --skip=0 --max-count=1`
	CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`

	if [ -z "${LATEST_REVISION}" ]
	then
		TAG_NAME="0.0.0"
	else
		TAG_NAME="`git describe --abbrev=0 --tags $LATEST_REVISION`"
	fi

	THIS_VERSION="${TAG_NAME%.*}.$((${TAG_NAME##*.}+1))"

	if [ $CURRENT_BRANCH == "master" ]
	then
	    THIS_VERSION=${TAG_NAME}
	else
    	THIS_VERSION="${TAG_NAME%.*}.$((${TAG_NAME##*.}+1))-SNAPSHOT"
	fi
	OUTPUT_VERSION="${THIS_VERSION}"
	echo "version [${OUTPUT_VERSION} ($CURRENT_BRANCH)]"
}

function install {
	if [ -z "${INSTALL_DIRECTORY}" ]
	then
		echo "please specify the target directory."
		exit 0
	fi

    mkdir -p ${INSTALL_DIRECTORY}/images
    cp -R ${SRC_DIR}/images/* ${INSTALL_DIRECTORY}/images/
    cp ${SRC_DIR}/*.sty ${INSTALL_DIRECTORY}/
}

function package {
    determineVersion
    mkdir -p ${OUTPUT_DIR}
    cd ${SRC_DIR}
    tar -cvf ${OUTPUT_DIR}/${OUTPUT_FILENAME}-${OUTPUT_VERSION}.tar.gz *.sty images/*
    cd ${ROOT_DIR}
}

case "$1" in
        clean)
            clean
	    	;;
		version)
			determineVersion
			;;
		package)
		    package
		    ;;
		install)
		    INSTALL_DIRECTORY=$2
		    install
		    ;;
        *)
            build
            ;;
esac
