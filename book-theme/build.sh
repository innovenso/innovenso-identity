#!/usr/bin/env bash

OUTPUT_FILENAME=innovenso-book
DEPLOY_BASE_TITLE="Innovenso Book"
DEPLOY_DESCRIPTION="Latest version"
SLACK_CHANNEL="job-jurgen"
OUTPUT_VERSION=0.0.1
ROOT_DIR=`pwd`
BUILD_DIR="${ROOT_DIR}/build"
OUTPUT_DIR="${ROOT_DIR}/output"
SRC_DIR="${ROOT_DIR}/src"
THEME_DIR="${ROOT_DIR}/theme"
LIB_DIR="${ROOT_DIR}/lib"
DOCUMENT_INPUT_FILE=document
PRESENTATION_INPUT_FILE=presentation
HANDOUT_INPUT_FILE=handouts
HANDOUT_WITH_NOTES_INPUT_FILE=handouts_with_notes
IMAGE_TESTER_INPUT_FILE=image_tester

function clean {
	rm -rf "${BUILD_DIR}"
	rm -rf "${OUTPUT_DIR}"
}

function buildSourceFile {
	determineVersion
	local INPUT_FILE=$1
	local OUTPUT_SUFFIX=$2
	rm -rf "${BUILD_DIR}"
	mkdir -p "${BUILD_DIR}"
	cp -R "${SRC_DIR}"/* "${BUILD_DIR}"
    cp -R "${THEME_DIR}"/* "${BUILD_DIR}"
    cp -R "${LIB_DIR}"/* "${BUILD_DIR}"
	cd "${BUILD_DIR}"
	latexmk -pdf "${INPUT_FILE}.tex"
	cd "${ROOT_DIR}"
	mkdir "${OUTPUT_DIR}"
	cp "$BUILD_DIR/${INPUT_FILE}.pdf" "${OUTPUT_DIR}/${OUTPUT_FILENAME}-${OUTPUT_SUFFIX}-${OUTPUT_VERSION}.pdf"
	rm -rf "${BUILD_DIR}"
}

function tikz {
    buildSourceFile ${IMAGE_TESTER_INPUT_FILE} "image"
}

function document {
    buildSourceFile ${DOCUMENT_INPUT_FILE} "document"
}

function build {
    document
}

function determineVersion {
	LATEST_REVISION=`git rev-list --tags --skip=0 --max-count=1`
	CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`

	if [ -z "${LATEST_REVISION}" ]
	then
		TAG_NAME="0.0.0"
	else
		TAG_NAME="`git describe --abbrev=0 --tags $LATEST_REVISION`"
    	DEPLOY_DESCRIPTION="`git tag -l --format='%(contents)' ${TAG_NAME}`"
	fi

	THIS_VERSION="${TAG_NAME%.*}.$((${TAG_NAME##*.}+1))"

	if [ $CURRENT_BRANCH == "master" ]
	then
	    THIS_VERSION=${TAG_NAME}
	else
    	THIS_VERSION="${TAG_NAME%.*}.$((${TAG_NAME##*.}+1))-SNAPSHOT"
	fi
	OUTPUT_VERSION="${THIS_VERSION}"
	echo "version [${OUTPUT_VERSION} ($CURRENT_BRANCH)]"
}

function deploy {
    if [ -z "${SLACK_CHANNEL}" ] || [ -z "${SLACK_TOKEN}" ]
    then
        echo "Missing environment variables: SLACK_CHANNEL and SLACK_TOKEN"
    else
        build
        DEPLOY_DOCUMENT_FILE="${OUTPUT_DIR}/${OUTPUT_FILENAME}-document-${OUTPUT_VERSION}.pdf"
        DEPLOY_HANDOUT_FILE="${OUTPUT_DIR}/${OUTPUT_FILENAME}-handout-${OUTPUT_VERSION}.pdf"
        curl -F file="@${DEPLOY_DOCUMENT_FILE}" -F title="${DEPLOY_BASE_TITLE} ${OUTPUT_VERSION} Document" -F initial_comment="Document: ${DEPLOY_DESCRIPTION}" -F channels=$SLACK_CHANNEL -F token=$SLACK_TOKEN https://slack.com/api/files.upload
        curl -F file="@${DEPLOY_HANDOUT_FILE}" -F title="${DEPLOY_BASE_TITLE} ${OUTPUT_VERSION} Summary Handout" -F initial_comment="Summary Handout: ${DEPLOY_DESCRIPTION}" -F channels=$SLACK_CHANNEL -F token=$SLACK_TOKEN https://slack.com/api/files.upload
    fi
}

function gitlog2latex {
	echo "Generating Revision log..."
	local REVLOG_OUTPUT="${SRC_DIR}/revision_log.tex"
	local REVISION_LOG_BUILD_DIR="${BUILD_DIR}/revision_log"
	local MD_FILE="${REVISION_LOG_BUILD_DIR}/gitlog1.md"
	local MD_FILE2="${REVISION_LOG_BUILD_DIR}/gitlog2.md"
	local TEX_FILE1="${REVISION_LOG_BUILD_DIR}/gitlog1.tex"
	local TEX_FILE2="${REVISION_LOG_BUILD_DIR}/gitlog2.tex"
	local TEX_FILE3="${REVISION_LOG_BUILD_DIR}/gitlog3.tex"
	local TEX_FILE4="${REVISION_LOG_BUILD_DIR}/gitlog4.tex"

	mkdir -p "${REVISION_LOG_BUILD_DIR}"

	git log --no-merges --pretty=format:"%an & %ad & %s
	" --date=short --decorate=full -- src/*.tex src/*/*.tex  > "${MD_FILE}"

	echo "\begin{center}
	\begingroup
	\renewcommand{\arraystretch}{1.5} % Default value: 1
	\begin{longtabu} to \textwidth {
	    X[4,l]
	    X[3,c]
	    X[8,l]}
	    \toprule
	    \textbf{Author} & \textbf{Date} & \textbf{Message} \\\\ \midrule " > "${REVLOG_OUTPUT}"

	awk '{printf "|%s\n",$0}' < "${MD_FILE}" >> "${MD_FILE2}"
	pandoc "${MD_FILE2}" -o "${TEX_FILE1}"
	tr '\n' ' ' < "${TEX_FILE1}" > "${TEX_FILE2}"
	sed -i -e 's/\\textbar{}/\'$'\n/g' "${TEX_FILE2}"
	sed '/^ $/d' "${TEX_FILE2}" > "${TEX_FILE3}"
	sed '/^$/d' "${TEX_FILE3}" > "${TEX_FILE2}"
	awk '{printf "%s\\\\ \n",$0}' < "${TEX_FILE2}" >> "${TEX_FILE4}"
	sed -i -e 's/\\&/\&/g' "${TEX_FILE4}"
	cat "${TEX_FILE4}" >> "${REVLOG_OUTPUT}"
	echo "\bottomrule \end{longtabu} \endgroup
	\end{center}" >> "${REVLOG_OUTPUT}"
	rm -rf "${REVISION_LOG_BUILD_DIR}"
	echo "Finished generating commit log!"
	echo "Include the file ${REVLOG_OUTPUT} in your main LaTeX source file."
	echo "Make sure to use the following packages:"
	echo "tabu"
	echo "array"
	echo "longtable"
}

case "$1" in
        clean)
            clean
	    	;;
		log)
			gitlog2latex
			;;
		deploy)
		    deploy
		    ;;
		version)
			determineVersion
			;;
		tikz)
		    tikz
		    ;;
		document)
		    document
		    ;;
        *)
            build
            ;;
esac